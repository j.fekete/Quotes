package com.unique.juraj.zadatak;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {
    @BindView(R.id.bJobs) Button bJobs;
    @BindView(R.id.bRitchie) Button bRitchie;
    @BindView(R.id.bBabbage) Button bBabbage;
    String[] jobs = {"Great things in business are never done by one person. They're done by a team of people."
            , "Computers themselves, and software yet to be developed, will revolutionize the way we learn."
            , "Apple took the edge off the word 'computer.'"
            , "I've always wanted to own and control the primary technology in everything we do."
    };

    String[] ritchie = {"UNIX is basically a simple operating system, but you have to be a genius to understand the simplicity."
            ,"For infrastructure technology, C will be hard to displace.","C++ and Java, say, are presumably growing faster than plain C, but I bet C will still be around.",
            "I'm not a person who particularly had heros when growing up."};

    String[] babbage = {"Errors using inadequate data are much less than those using no data at all.","Whenever the work is itself light, it becomes necessary, in order to economize time, to increase the velocity."
            ,"In mathematics we have long since drawn the rein, and given over a hopeless race."
            ,"Perhaps it would be better for science, that all criticism should be avowed."
            ,"The economy of human time is the next advantage of machinery in manufactures."};
    Random rndGenerator=new Random();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int r = rndGenerator.nextInt(10);
        ButterKnife.bind(this);


    }
    @OnClick({R.id.bJobs,R.id.bRitchie,R.id.bBabbage})
    public void displayQuote(View view){
        String quote = "";
        switch (view.getId())
        {
            case R.id.bJobs:
                quote = this.jobs[this.rndGenerator.nextInt(this.jobs.length)];
                break;
        case R.id.bRitchie:
            quote = this.ritchie[this.rndGenerator.nextInt(this.ritchie.length)];
            break;
            case R.id.bBabbage:
                quote = this.babbage[this.rndGenerator.nextInt(this.babbage.length)];
                break;
    }
    this.toastQuote(quote);
    }

    private void toastQuote(String quote) {
        Toast toast = Toast.makeText(this,quote,Toast.LENGTH_LONG);
        toast.show();
    }

}
